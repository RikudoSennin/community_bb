# CommunityBB #

This project aims to provide a clean, open source forum, with a community based moderation.

## Features: ##

 - Reputation gaining based on community voting
 - Allow for normal moderation as well as community moderation
 - Easy and Clean, highly extensible.
 - Community based development.

## Gameplan ##

### Pages ###

 - **Portal**, to display recent posts and widgets. Index page (or configurable `/portal/`)
 - **Forum tree page**, to display a forum list.  `/tree/[{category-ID}/{lowercase-dashed-name}]` (or configurable Index page)
 - **Forum display page**, to display a forum's inner content, all posts and such,
   as well as any subforums. `forum/{id}/{lowercase-dashed-name}`

   Each post will have:
    1. Title (Link to original post)
    2. Some content [?] (Link to original post)
    3. Time of OP (Link to original post)
    4. Name of OP (Link to poster profile)
    5. Time of last change (Link to latest change)
    6. Name of last changer (Link to last changer profile)

 - **Single post page**, to display full content of a post and all responses. `/post/{post-ID}/{lowercase-dashed-name}`

   Orderable by:
    - Time (default, descending)
    - Votes

 - **User profile**, to display misc data and stats on a user. `/user/{user-ID}/{lowercase-dashed-name}`

   - **User edit page**. `/edit-my-profile/`

 - **Administration Panel**. More on that later :D `/admin/`
