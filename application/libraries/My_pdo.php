<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Since CI doesn't provide us with a PDO class, I'll just go ahead and make one.
 */
class My_pdo {

    /**
     * @var string Hostname
     */
    private $host = "localhost";
    /**
     * @var string Username; REQUIRED!
     */
    private $username; //REQUIRED!
    /**
     * @var string Password; REQUIRED!
     */
    private $password; //REQUIRED!
    /**
     * @var string Database Type.
     */
    private $databaseType = "mysql";
    /**
     * @var string Database Name.
     */
    private $databaseName; //REQUIRED!

    /**
     * @var PDO holds connection object
     */
    private $conn;

    /**
     * Constructor: Initiate the library with the $config array
     * @param array $config
     */
    public function __construct($config = array()) {
        if (count($config) > 0) {
            $this->init($config);
        }
        $this->connect();
    }

    /**
     * Initialize all variables.
     * @param $config
     * @return \Mypdo
     */
    public function init($config) {
        foreach ($config as $key => $value) {
            $method = "set_$key";
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
            else {
                $this->$key = $value;
            }
        }

        //return $this; //Don't break the chain!

    }

    public function connect() {
        try { //to connect
            $dsn = "$this->databaseType:host=$this->host;dbname=$this->databaseName";
            $this->conn = new PDO($dsn, $this->username, $this->password);
            //$this->conn = new PDO("mysql:host=localhost;dbname=test", "root", "1499823");

            echo $dsn;

            $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $e) {
            die("There was a problem with PDO! " . $e->getMessage());
        }
    }

    /**
     * Get the connection object.
     * I prefer working with PDO directly, rather than having a library do the dirty work.
     * @return \PDO
     */
    public function getInstance() {
        if (!($this->conn instanceof PDO)) {
            $this->connect();
        }
        /*echo "<pre>";
        var_dump(debug_backtrace());
        echo "</pre>";*/
        return $this->conn;
    }

}