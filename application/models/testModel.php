<?php

/**
 * @property \My_pdo my_pdo
 * @property \CI_Loader load
 */
class TestModel extends CI_Model {
    public function testDB() {
        $this->load->library(
            "my_pdo",
            array(
                 "username" => "root",
                 "password" => "1499823",
                 "databaseName" => "test"
            )
        );
        $pdo = $this->my_pdo->getInstance();
        $query = <<<SQL
INSERT INTO `users`
(
  `uname`,
  `phash`,
  `id`
)
VALUES
(
  'omg',
  'wtf',
  NULL
);

SQL;

        $pdo->query($query);
    }
}